
import sys, getopt, math, os
import ROOT

######### Read input parameters and open file ##############
f_in = []
f_out = None
tree_name = None
isData = 0
events_before_presel = None
weighted_events_before_presel = None

def printHelp():
  print("\nThis script takes three arguments: \n \n")
  print("      -i   Input root file.")
  print("      -o   Name of the output file with cutflow table produced in folder ./tex.")
  print("      -t   Name of the TTree in the input ROOT file containing the data .")
  print("      -d   If the file is data 'd' otherwise 's', don't look into SR.")
  print("      -n   Total number of event before preselection. Relevant only for signal and background.")
  print("      -w   Weighted number of events before preselection.")


try:
	opts, args = getopt.getopt(sys.argv[1:],"i:o:t:d:n:w:")
except getopt.GetoptError:
	print("Something went wrong. Check input parameters.")

for opt, arg in opts:
	print(opt, "-", arg)
	if opt == "-i":
		f_in.append(arg)
	if opt == "-o":
		f_out = arg
	if opt == "-t":
		tree_name = arg
	if opt == "-d":
		#isData = bool(arg)
		isData = arg
		if not (arg == "s" or arg == "d"):
			print("ERROR: Parameter -d can be only s or d")
			printHelp()
			sys.exit()
	if opt == "-n":
		events_before_presel = int(arg)
	if opt == "-w":
		weighted_events_before_presel = float(arg)

if (isData == "s") and (events_before_presel == None or weighted_events_before_presel == None):
	print("ERROR: Enter number and weighted number of events before preselection. Parameters -n and -w.")
	printHelp()
	sys.exit()

if f_in == None or f_out == None or tree_name == None or isData == None:
	print("ERROR: At least three arguments have to be provided. Stopping script.")
	printHelp()
	sys.exit()

stop_fraction = input("What fraction of the file should script loop through: \n")

t = ROOT.TChain(tree_name)

for input_file in f_in:
	t.Add(input_file)


############ Definition of counter variables #################
events_total = t.GetEntries()

cut_1 = 0
cut_2 = 0
cut_3 = 0
cut_4 = 0
cut_5 = 0
cut_6 = 0
cut_7 = 0
cut_8 = 0

weighted_events_total = 0
weighted_cut_1 = 0
weighted_cut_2 = 0
weighted_cut_3 = 0
weighted_cut_4 = 0
weighted_cut_5 = 0
weighted_cut_6 = 0
weighted_cut_7 = 0
weighted_cut_8 = 0

weight_btag = None
weight_elec = None
weight_elec_trigger = None
weight_jvt = None
weight_mc = None
weight_muon = None
weight_prescale = None
weight_pu = None
weight_lumi_real = None
weight_TOTAL = None

counter = 0


##################### Progress bar #########################
def progressBar(iterable, fraction = 1, prefix = '', suffix = '', decimals = 1, length = 100, fill = 'O', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iterable    - Required  : iterable object (Iterable)
				fraction    - Required  : fraction of iterable object to loop through
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    total = math.floor(iterable.GetEntries() * float( fraction ))
    # Progress Bar Printing Function
    def printProgressBar (iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / total ))
        filledLength = int(length * iteration // total )
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Initial Call
    printProgressBar(0)
    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()


################ Loop over input data ###################
#for event in t:
for event in progressBar(t, stop_fraction):
	counter += 1
	if (counter > float(stop_fraction) * events_total):
		break

	if isData == "s":
		weight_btag = t.weight_btag
		weight_elec = t.weight_elec
		weight_jvt = t.weight_jvt
		weight_mc = t.weight_mc
		weight_muon = t.weight_muon
		weight_prescale = t.weight_prescale
		weight_pu = t.weight_pu
		#weight_lumi_real = t.weight_lumi_real
		weight_lumi_real = 4 * t.weight_lumi*(36207.66*(t.run_number<324320) + 44307.4*(t.run_number>=324320 and t.run_number <348885) + 58450.1*(t.run_number>=348885))
		
		if weight_prescale != 1:
			print("WARNING: Prescale weight not equal to 1!")

		weight_TOTAL = (weight_btag *  weight_elec *
										weight_jvt * weight_mc * weight_muon * 
										weight_prescale * weight_pu * weight_lumi_real)

	if isData == "d":
		weight_lumi_real = 1
		weight_TOTAL = 1

	weighted_events_total += weight_TOTAL

	############ Applying cuts on data ###############
	if isData == "s":
		#hh_type_condition = (t.hh_type == 10)
		hh_type_condition = (t.hh_type == 10 or t.hh_type == 11 or t.hh_type == 12)
	if isData == "d":
		hh_type_condition = True
		#hh_type_condition = (t.m_h1_rand_min_dR_ptlead > 0 and t.m_h2_rand_min_dR_ptlead > 0) #We want only events with these variables not equal to 0 for data - this "cut" is not reported for the data
	
	# Cut 1
	if hh_type_condition:
		cut_1 += 1	
		weighted_cut_1 += weight_TOTAL

		# Cut 2
		if t.pass_bjet_grl == 1:
			cut_2 += 1
			weighted_cut_2 += weight_TOTAL		

			# Cut 3. Runs from 2016	
			if t.run_number >= 296939 and t.run_number <= 311481: 
				cut_3 += 1
				weighted_cut_3 += weight_TOTAL

				trig_ht = t.pass_HLT_2j55_bmv2c2060_split_ht300_L14J15 
				trig_isr = t.pass_HLT_j100_2j55_bmv2c2060_split
				t.SetAlias("pass_HLT_2j35_bmv2c2060_split_2j35_L14J150ETA25","pass_HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25")
				trig_2b = t.pass_HLT_2j35_bmv2c2060_split_2j35_L14J150ETA25
				trigger_condition = ((t.pt_jet_1 > 150) and trig_isr and t.matchedTriggerHashesA) or \
														((not t.pt_jet_1 > 150) and (t.ht > 99999999999999999999) and trig_ht and t.matchedTriggerHashesC) or \
														((not t.pt_jet_1 > 150) and (not t.ht > 99999999999999999999) and trig_2b and t.matchedTriggerHashesB)
			
				# Cut 4
				if trigger_condition:
					cut_4 += 1
					weighted_cut_4 +=weight_TOTAL
				
					if isData == "s":
						bjet_condition = (t.bjets_n_pt40 >= 4)
					if isData == "d":
						bjet_condition = (t.bjets_n_pt40 == 2)
					
					# Cut 5
					if bjet_condition:
						cut_5 += 1
						weighted_cut_5 += weight_TOTAL
						lepton_condition = (t.signal_leptons_n == 0 and t.baseline_preor_med_leptons_n <= 1 and
																t.baseline_preor_loose_leptons_n <= 2 )
						# Cut 6
						if lepton_condition:
							cut_6 += 1
							weighted_cut_6 += weight_TOTAL

							# Cut 7
							if t.xwt_rand >= 1.8:
								cut_7 += 1
								weighted_cut_7 += weight_TOTAL
								
								if (t.m_h1_rand_min_dR_ptlead > 0 and t.m_h2_rand_min_dR_ptlead > 0):
									SR_condition = (((((t.m_h1_rand_min_dR_ptlead-120.) / (0.1*t.m_h1_rand_min_dR_ptlead))**2 + 
																	((t.m_h2_rand_min_dR_ptlead-110.) / (0.1*t.m_h2_rand_min_dR_ptlead))**2)**0.5) < 1.6)
									# Cut 8
									if SR_condition:
										cut_8 += 1
										weighted_cut_8 += weight_TOTAL
									
								
			# Cut 3. Runs from 2017
			if (t.run_number >= 324320) and (t.run_number <= 341649): 
				cut_3 += 1
				weighted_cut_3 += weight_TOTAL
				
				if isData == "s":
					t.SetAlias("pass_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5ETA21","pass_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21")
				if isData == "d":
					t.SetAlias("pass_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5ETA21","pass_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5.ETA21")
				trig_ht = t.pass_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5ETA21
				trig_isr = t.pass_HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30
				t.SetAlias("pass_HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J150ETA25","pass_HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25")
				trig_2b = t.pass_HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J150ETA25
				trigger_condition = ((t.pt_jet_1 > 350) and trig_isr and t.matchedTriggerHashesA) or \
														((not t.pt_jet_1 > 350) and (t.ht > 850) and trig_ht and t.matchedTriggerHashesC) or \
														((not t.pt_jet_1 > 350) and (not t.ht > 850) and trig_2b and t.matchedTriggerHashesB)
				
				# Cut 4
				if trigger_condition:
					cut_4 += 1
					weighted_cut_4 += weight_TOTAL
				
					if isData == "s":
						bjet_condition = (t.bjets_n_pt40 >= 4)
					if isData == "d":
						bjet_condition = (t.bjets_n_pt40 == 2)
					
					# Cut 5
					if bjet_condition:
						cut_5 += 1
						weighted_cut_5 += weight_TOTAL

						lepton_condition = (t.signal_leptons_n == 0 and t.baseline_preor_med_leptons_n <= 1 and
																t.baseline_preor_loose_leptons_n <= 2 )
						# Cut 6
						if lepton_condition:
							cut_6 += 1
							weighted_cut_6 += weight_TOTAL

							# Cut 7
							if t.xwt_rand >= 1.8:
								cut_7 += 1
								weighted_cut_7 += weight_TOTAL
								
								if (t.m_h1_rand_min_dR_ptlead > 0 and t.m_h2_rand_min_dR_ptlead > 0):
									SR_condition = (((((t.m_h1_rand_min_dR_ptlead-120.) / (0.1*t.m_h1_rand_min_dR_ptlead))**2 + 
																	((t.m_h2_rand_min_dR_ptlead-110.) / (0.1*t.m_h2_rand_min_dR_ptlead))**2)**0.5) < 1.6)
									# Cut 8
									if SR_condition:
										cut_8 += 1
										weighted_cut_8 += weight_TOTAL
				

			# Cut 3. Runs from 2018
			if t.run_number >= 348197 and t.run_number <= 364485: 
				cut_3 += 1
				weighted_cut_3 += weight_TOTAL

				if isData == "s":
					t.SetAlias("pass_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5ETA21","pass_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21")
				if isData == "d":
					t.SetAlias("pass_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5ETA21","pass_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5.ETA21")
				trig_ht = t.pass_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5ETA21
				trig_isr = t.pass_HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30
				t.SetAlias("pass_HLT_2j35_bmv2c1060_split_2j35_L14J150ETA25","pass_HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25")
				trig_2b = t.pass_HLT_2j35_bmv2c1060_split_2j35_L14J150ETA25
				trigger_condition = ((t.pt_jet_1 > 500) and trig_isr and t.matchedTriggerHashesA) or \
														((not t.pt_jet_1 > 500) and (t.ht > 700) and trig_ht and t.matchedTriggerHashesC) or \
														((not t.pt_jet_1 > 500) and (not t.ht > 700) and trig_2b and t.matchedTriggerHashesB)
				
				# Cut 4
				if trigger_condition:
					cut_4 += 1
					weighted_cut_4 += weight_TOTAL
				
					if isData == "s":
						bjet_condition = (t.bjets_n_pt40 >= 4)
					if isData == "d":
						bjet_condition = (t.bjets_n_pt40 == 2)
					
					# Cut 5
					if bjet_condition:
						cut_5 += 1
						weighted_cut_5 += weight_TOTAL

						lepton_condition = (t.signal_leptons_n == 0 and t.baseline_preor_med_leptons_n <= 1 and
																t.baseline_preor_loose_leptons_n <= 2 )
						# Cut 6
						if lepton_condition:
							cut_6 += 1
							weighted_cut_6 += weight_TOTAL

							# Cut 7
							if t.xwt_rand >= 1.8:
								cut_7 += 1
								weighted_cut_7 += weight_TOTAL
								
								if ((t.m_h1_rand_min_dR_ptlead > 0 and t.m_h2_rand_min_dR_ptlead > 0)): 
									SR_condition = (((((t.m_h1_rand_min_dR_ptlead-120.) / (0.1*t.m_h1_rand_min_dR_ptlead))**2 + 
																((t.m_h2_rand_min_dR_ptlead-110.) / (0.1*t.m_h2_rand_min_dR_ptlead))**2)**0.5) < 1.6)
									# Cut 8
									if SR_condition:
										cut_8 += 1
										weighted_cut_8 += weight_TOTAL


################### Print results to output file ####################### 
label_0 = "Total number of events"
label_00 = "Events after preselection"
label_cut_1 = "hh decays"
label_cut_2 = "good run list"
label_cut_3 = "run numbers"
label_cut_4 = "trigger"
label_cut_5 = "b jets"
label_cut_6 = "lepton veto"
label_cut_7 = "top veto"
label_cut_8 = "TCut SR"

if isData == "s":
  headers = [" ", 														"Unweighted", 													"Unweighted \%",																				"Weighted", 																	"Weighted \%"																														]
  table = [[label_0,													str(round(events_before_presel)),			str(round(100*events_before_presel/events_before_presel, 4)),	str(round(weighted_events_before_presel, 3)),	str(round(100*weighted_events_before_presel/weighted_events_before_presel, 4))	],
           [label_00,													str(round(events_total, 0)),					str(round(100*events_total/events_before_presel, 4)),					str(round(weighted_events_total, 3)),					str(round(100*weighted_events_total/weighted_events_before_presel, 4))					],
           [label_cut_1,											str(round(cut_1, 0)),									str(round(100*cut_1/events_before_presel,4)),									str(round(weighted_cut_1,3)),									str(round(100*weighted_cut_1/weighted_events_before_presel, 4))									],	
           [label_cut_2,											str(round(cut_2, 0)),									str(round(100*cut_2/events_before_presel,4)),									str(round(weighted_cut_2,3)),									str(round(100*weighted_cut_2/weighted_events_before_presel, 4))									],	
           [label_cut_3,											str(round(cut_3, 0)),									str(round(100*cut_3/events_before_presel,4)),									str(round(weighted_cut_3,3)),									str(round(100*weighted_cut_3/weighted_events_before_presel, 4))									],	
           [label_cut_4,											str(round(cut_4, 0)),									str(round(100*cut_4/events_before_presel,4)),									str(round(weighted_cut_4,3)),									str(round(100*weighted_cut_4/weighted_events_before_presel, 4))									],	
           [label_cut_5,											str(round(cut_5, 0)),									str(round(100*cut_5/events_before_presel,4)),									str(round(weighted_cut_5,3)),									str(round(100*weighted_cut_5/weighted_events_before_presel, 4))									],	
           [label_cut_6,											str(round(cut_6, 0)),									str(round(100*cut_6/events_before_presel,4)),									str(round(weighted_cut_6,3)),									str(round(100*weighted_cut_6/weighted_events_before_presel, 4))									],	
           [label_cut_7,											str(round(cut_7, 0)),									str(round(100*cut_7/events_before_presel,4)),									str(round(weighted_cut_7,3)),									str(round(100*weighted_cut_7/weighted_events_before_presel, 4))									], 	
           [label_cut_8,									    str(round(cut_8, 0)),									str(round(100*cut_8/events_before_presel,4)),			        		str(round(weighted_cut_8,1)),									str(round(100*weighted_cut_8/weighted_events_before_presel, 4))			   					]]
if isData == "d":
  headers = [" ",                             "Unweighted",                         "Unweighted \%",                                     ]
  table =  [[label_0,									        str(round(events_total, 0)),          str(round(100*events_total/events_total, 4))         ],
          #[label_cut_1,                      str(round(cut_1, 0)),                 str(round(100*cut_1/events_total,2))                 ], This cut is not relevant for data
           [label_cut_2,                      str(round(cut_2, 0)),                 str(round(100*cut_2/events_total,4))                 ],
           [label_cut_3,                      str(round(cut_3, 0)),                 str(round(100*cut_3/events_total,4))                 ],
           [label_cut_4,                      str(round(cut_4, 0)),                 str(round(100*cut_4/events_total,4))                 ],
           [label_cut_5,                      str(round(cut_5, 0)),                 str(round(100*cut_5/events_total,4))                 ],
           [label_cut_6,                      str(round(cut_6, 0)),                 str(round(100*cut_6/events_total,4))                 ],
           [label_cut_7,                      str(round(cut_7, 0)),                 str(round(100*cut_7/events_total,4))                 ],
           [label_cut_8,                      str(round(cut_8, 0)),                 str(round(100*cut_8/events_total,4))                 ]]

from tabulate import tabulate
print()
print(tabulate(table, headers, tablefmt="latex_raw", floatfmt=("0.f",".0f", ".3e", ".3f", ".3e")))

output = open("./tex/table.tex", "w")
output.writelines(tabulate(table, headers, tablefmt="latex_raw"))
output.writelines("\\vspace{15pt}")
output.writelines("\n\n {\\tiny Using files: \n\n")
for f in f_in:
	output.writelines(f.replace("_"," \\textunderscore ") + '\n\n')
output.writelines("\n\n Using TTree: " + tree_name.replace("_"," \\textunderscore "))
output.writelines("}")
output.close()

os.chdir("./tex/")
os.system("pdflatex ./main.tex > /dev/null 2>&1")
command = "mv main.pdf " + f_out + ".pdf"
os.system(command)
command = "cp table.tex " + f_out + ".tex"
os.system(command)

print("counter: ", counter)
print("events: ", events_total)
