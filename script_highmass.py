
import sys, getopt, math, os
import ROOT

f_in = []
f_out = None
tree_name = None
dataType = ""
events_before_presel = None
weighted_events_before_presel = None

def printHelp():
	print("\nThis script takes three arguments: \n \n")
	print("      -i    Input root file.")
	print("      -o    Name of the output file with cutflow table produced in folder ./tex.")
	print("      -t    Name of the TTree in the input ROOT file containing the data .")
	print("      -d   Data type: s = signal, b = background, d = data")
	print("      -n   Total number of event before preselection. Relevant only for signal and background.")
	print("      -w   Weighted number of events before preselection.")

try:
	opts, args = getopt.getopt(sys.argv[1:],"i:o:t:d:n:w:")
except getopt.GetoptError:
	print("Something went wrong. Check input parameters.")

for opt, arg in opts:
	print(opt, "-", arg)
	if opt == "-i":
		f_in.append(arg)
	if opt == "-o":
		f_out = arg
	if opt == "-t":
		tree_name = arg
	if opt == "-d":
		dataType = arg
		if not (dataType == "s" or dataType == "b" or dataType == "d"):
			print("ERROR: Wrong data type.") 
			printHelp()
			sys.exit()
	if opt == "-n":
		events_before_presel = int(arg)
	if opt == "-w":
		weighted_events_before_presel = float(arg)
		
if (dataType == "s" or dataType == "b") and (events_before_presel == None or weighted_events_before_presel == None):
	print("ERROR: Enter number and weighted number of events before preselection. Parameters -n and -w.")
	printHelp()
	sys.exit()

if f_in == None or f_out == None or tree_name == None:
	print("ERROR: Three arguments have to be provided. Stopping script.")
	printHelp()
	sys.exit()

stop_fraction = input("What fraction of the file should script loop through: \n")

t = ROOT.TChain(tree_name)

for input_file in f_in:
  t.Add(input_file)


############ Definition of counter variables #################
events_total = t.GetEntries()

events_pass_met = 0
events_cut_hh = 0
events_cut_lep = 0
events_cut_pflowJets = 0
events_cut_3bJets = 0
events_cut_EtMiss = 0
events_cut_dphimin = 0

GGMH_HM_BDT_200 = 0
GGMH_HM_BDT_250 = 0
GGMH_HM_BDT_300 = 0
GGMH_HM_BDT_400 = 0
GGMH_HM_BDT_500 = 0
GGMH_HM_BDT_600 = 0
GGMH_HM_BDT_700 = 0
GGMH_HM_BDT_800 = 0
GGMH_HM_BDT_900 = 0
GGMH_HM_BDT_1000 = 0
GGMH_HM_BDT_1100 = 0

weighted_events_pass_met = 0
weighted_events_total = 0
weighted_events_cut_hh = 0
weighted_events_cut_lep = 0
weighted_events_cut_pflowJets = 0
weighted_events_cut_3bJets = 0
weighted_events_cut_EtMiss = 0
weighted_events_cut_dphimin = 0

weighted_GGMH_HM_BDT_200 = 0
weighted_GGMH_HM_BDT_250 = 0
weighted_GGMH_HM_BDT_300 = 0
weighted_GGMH_HM_BDT_400 = 0
weighted_GGMH_HM_BDT_500 = 0
weighted_GGMH_HM_BDT_600 = 0
weighted_GGMH_HM_BDT_700 = 0
weighted_GGMH_HM_BDT_800 = 0
weighted_GGMH_HM_BDT_900 = 0
weighted_GGMH_HM_BDT_1000 = 0
weighted_GGMH_HM_BDT_1100 = 0

weight_btag = None
weight_elec = None
weight_elec_trigger = None
weight_jvt = None
weight_mc = None
weight_muon = None
weight_prescale = None
weight_pu = None
weight_lumi_real = None
weight_TOTAL = None

counter = 0

##################### Progress bar #########################
def progressBar(iterable, fraction = 1, prefix = '', suffix = '', decimals = 1, length = 100, fill = 'O', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iterable    - Required  : iterable object (Iterable)
        fraction    - Required  : fraction of iterable object to loop through
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    total = math.floor(iterable.GetEntries() * float( fraction ))
    print("Totla: ", total)
    # Progress Bar Printing Function
    def printProgressBar (iteration):
        percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / total ))
        filledLength = int(length * iteration // total )
        bar = fill * filledLength + '-' * (length - filledLength)
        print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Initial Call
    printProgressBar(0)
    # Update Progress Bar
    for i, item in enumerate(iterable):
        yield item
        printProgressBar(i + 1)
    # Print New Line on Complete
    print()

################ Loop over input data ###################
#for event in t:
for event in progressBar(t, stop_fraction):
	counter += 1
	if (counter > float(stop_fraction) * events_total):
		break

	if dataType == "s" or dataType == "b":
		weight_btag = t.weight_btag
		weight_elec = t.weight_elec
		weight_jvt = t.weight_jvt
		weight_mc = t.weight_mc
		weight_muon = t.weight_muon
		weight_pu = t.weight_pu
		weight_met_trig_correct = t.weight_met_trig_correct
		#weight_met_trig_correct = 1
		#weight_lumi_real = t.weight_lumi_real
		weight_lumi_real = t.weight_lumi*(36207.66*(t.run_number<324320) + 44307.4*((t.run_number>=324320) and (t.run_number <348885)) + 58450.1*(t.run_number>=348885))

  ############ Applying cuts on data ###############
	if dataType == "s":
		condition_hh_type = (t.hh_type == 10 or t.hh_type==11 or t.hh_type==12 )
		weight_lumi_real = 4 * weight_lumi_real #TODO: Is this correct? Correction factor for the luminosity 
		weight_TOTAL = ( weight_btag *  weight_elec * weight_met_trig_correct *
									weight_jvt * weight_mc * weight_muon *
									weight_pu * weight_lumi_real)
		weighted_events_total += weight_TOTAL	
	if dataType == "b":
		condition_hh_type = True
		weight_TOTAL = (weight_btag *  weight_elec * weight_met_trig_correct * 
									weight_jvt * weight_mc * weight_muon *
								  weight_pu * weight_lumi_real)
		weighted_events_total += weight_TOTAL	
	if dataType == "d":
		condition_hh_type = True
		weight_TOTAL = 1
		weight_lumi_real = 1
		weighted_events_total = 1
		

	# Cut 1
	if condition_hh_type:
		events_cut_hh += 1
		weighted_events_cut_hh += weight_TOTAL
		#print("Cut1: ", events_cut_hh)

		# Cut 2
		if t.pass_MET == 1:
			events_pass_met += 1
			weighted_events_pass_met += weight_TOTAL

			# Cut3
			if ((t.signal_electrons_n == 0) and (t.signal_muons_n == 0) and (t.signal_leptons_n == 0 ) and (t.baseline_preor_med_leptons_n <= 1)):
				events_cut_lep += 1	
				weighted_events_cut_lep += weight_TOTAL									
						
				# Cut 4	
				if t.jets_n >= 4 and t.jets_n <= 7:
					events_cut_pflowJets += 1
					weighted_events_cut_pflowJets += weight_TOTAL

					#Cut 5
					if t.bjets_n >= 3:
						events_cut_3bJets += 1
						weighted_events_cut_3bJets += weight_TOTAL

						#Cut 6
						if t.met >=150:
							events_cut_EtMiss += 1
							weighted_events_cut_EtMiss += weight_TOTAL					

							#Cut 7
							if t.dphi_min > 0.4:
								events_cut_dphimin += 1
								weighted_events_cut_dphimin += weight_TOTAL

								#Cut 8
								if t.GGMH_HM_BDT_200 > 0.99974 and t.GGMH_HM_BDT_200 <= 1.0000:
									GGMH_HM_BDT_200 += 1
									weighted_GGMH_HM_BDT_200 += weight_TOTAL
								
								if t.GGMH_HM_BDT_250 > 0.9997 and t.GGMH_HM_BDT_250 <= 1.0000:
									GGMH_HM_BDT_250 += 1
									weighted_GGMH_HM_BDT_250 += weight_TOTAL

								if t.GGMH_HM_BDT_300 > 0.99956 and t.GGMH_HM_BDT_300 <= 1.0000:
									GGMH_HM_BDT_300 += 1
									weighted_GGMH_HM_BDT_300 += weight_TOTAL

								if t.GGMH_HM_BDT_400 > 0.99974 and t.GGMH_HM_BDT_400 <= 1.0000:
									GGMH_HM_BDT_400 += 1
									weighted_GGMH_HM_BDT_400 += weight_TOTAL
								
								if t.GGMH_HM_BDT_500 > 0.9997 and t.GGMH_HM_BDT_500 <= 1.0000:
									GGMH_HM_BDT_500 += 1
									weighted_GGMH_HM_BDT_500 += weight_TOTAL

								if t.GGMH_HM_BDT_600 > 0.99974 and t.GGMH_HM_BDT_600 <= 1.0000:
									GGMH_HM_BDT_600 += 1
									weighted_GGMH_HM_BDT_600 += weight_TOTAL

								if t.GGMH_HM_BDT_700 > 0.99966 and t.GGMH_HM_BDT_700 <= 1.0000:
									GGMH_HM_BDT_700 += 1
									weighted_GGMH_HM_BDT_700 += weight_TOTAL

								if t.GGMH_HM_BDT_800 > 0.99948 and t.GGMH_HM_BDT_800 <= 1.0000:
									GGMH_HM_BDT_800 += 1
									weighted_GGMH_HM_BDT_800 += weight_TOTAL

								if t.GGMH_HM_BDT_900 > 0.99924 and t.GGMH_HM_BDT_900 <= 1.0000:
									GGMH_HM_BDT_900 += 1
									weighted_GGMH_HM_BDT_900 += weight_TOTAL

								if t.GGMH_HM_BDT_1000 > 0.9991 and t.GGMH_HM_BDT_1000 <= 1.0000:
									GGMH_HM_BDT_1000 += 1
									weighted_GGMH_HM_BDT_1000 += weight_TOTAL

								if t.GGMH_HM_BDT_1100 > 0.99912 and t.GGMH_HM_BDT_1100 <= 1.0000:
									GGMH_HM_BDT_1100 += 1
									weighted_GGMH_HM_BDT_1100 += weight_TOTAL



################### Print results to output file #######################
label_0 = "Total number of events"
label_00 = "Events after preselection"
label_cut_1 = "hh$\longrightarrow$4b decays"
label_cut_2 = "Events pass MET"
label_cut_3 = "Lepton veto"
label_cut_4 = "$>$3 and $<$8 jets"
label_cut_5 = "$>$2 b-jets"
label_cut_6 = "MET $>$ 150 GeV"
label_cut_7 = "$\\Delta \Phi_{min}^{4j} > 0.4$"
label_cut_8_1 = "GGMH\_HM\_BDT\_200"
label_cut_8_2 = "GGMH\_HM\_BDT\_250"
label_cut_8_3 = "GGMH\_HM\_BDT\_300"
label_cut_8_4 = "GGMH\_HM\_BDT\_400"
label_cut_8_5 = "GGMH\_HM\_BDT\_500"
label_cut_8_6 = "GGMH\_HM\_BDT\_600"
label_cut_8_7 = "GGMH\_HM\_BDT\_700"
label_cut_8_8 = "GGMH\_HM\_BDT\_800"
label_cut_8_9 = "GGMH\_HM\_BDT\_900"
label_cut_8_10 ="GGMH\_HM\_BDT\_1000"

####Converts number to string and puts it to required precision#####
def n2s(number, precision):
  if number >= eval(f"1e{-precision}"):
    s = "{:.{precision}f}".format(number, precision = precision)
  else:
    s = "{:.{precision}e}".format(number, precision = precision)
  return s


if dataType == "s":
	headers = [" ",                     "Unweighted",                  "Unweighted \%",                                        "Weighted",                              "Weighted \%"                                                             ]
	table = [[label_0,                  n2s(events_before_presel, 0),  n2s(100*events_before_presel/events_before_presel, 3),   n2s(weighted_events_before_presel, 3),   n2s(100*weighted_events_before_presel/weighted_events_before_presel, 3)  ],
					 [label_00,                 n2s(events_total, 0),          n2s(100*events_total/events_before_presel, 3),           n2s(weighted_events_total, 3),           n2s(100*weighted_events_total/weighted_events_before_presel, 3)          ],
					 [label_cut_1,              n2s(events_cut_hh, 0),         n2s(100*events_cut_hh/events_before_presel, 3),          n2s(weighted_events_cut_hh, 3),          n2s(100*weighted_events_cut_hh/weighted_events_before_presel, 3)         ],
					 [label_cut_2,              n2s(events_pass_met, 0),       n2s(100*events_pass_met/events_before_presel, 3),        n2s(weighted_events_pass_met, 3),        n2s(100*weighted_events_pass_met/weighted_events_before_presel, 3)       ],
					 [label_cut_3,              n2s(events_cut_lep, 0),        n2s(100*events_cut_lep/events_before_presel, 3),         n2s(weighted_events_cut_lep, 3),         n2s(100*weighted_events_cut_lep/weighted_events_before_presel, 3)        ],
					 [label_cut_4,              n2s(events_cut_pflowJets, 0),  n2s(100*events_cut_pflowJets/events_before_presel, 3),   n2s(weighted_events_cut_pflowJets, 3),   n2s(100*weighted_events_cut_pflowJets/weighted_events_before_presel, 3)  ],
					 [label_cut_5,              n2s(events_cut_3bJets, 0),     n2s(100*events_cut_3bJets/events_before_presel, 3),      n2s(weighted_events_cut_3bJets, 3),      n2s(100*weighted_events_cut_3bJets/weighted_events_before_presel, 3)     ],
					 [label_cut_6,              n2s(events_cut_EtMiss, 0),     n2s(100*events_cut_EtMiss/events_before_presel, 3),      n2s(weighted_events_cut_EtMiss, 3),      n2s(100*weighted_events_cut_EtMiss/weighted_events_before_presel, 3)     ],
					 [label_cut_7,              n2s(events_cut_dphimin, 0),    n2s(100*events_cut_dphimin/events_before_presel, 3),     n2s(weighted_events_cut_dphimin, 3),     n2s(100*weighted_events_cut_dphimin/weighted_events_before_presel, 3)    ],
					 [label_cut_8_1,            n2s(GGMH_HM_BDT_200, 0),       n2s(100*GGMH_HM_BDT_200/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_200, 3),        n2s(100*weighted_GGMH_HM_BDT_200/weighted_events_before_presel, 3)       ],
					 [label_cut_8_2,            n2s(GGMH_HM_BDT_250, 0),       n2s(100*GGMH_HM_BDT_250/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_250, 3),        n2s(100*weighted_GGMH_HM_BDT_250/weighted_events_before_presel, 3)       ],
					 [label_cut_8_3,            n2s(GGMH_HM_BDT_300, 0),       n2s(100*GGMH_HM_BDT_300/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_300, 3),        n2s(100*weighted_GGMH_HM_BDT_300/weighted_events_before_presel, 3)       ],
					 [label_cut_8_4,            n2s(GGMH_HM_BDT_400, 0),       n2s(100*GGMH_HM_BDT_400/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_400, 3),        n2s(100*weighted_GGMH_HM_BDT_400/weighted_events_before_presel, 3)       ],
					 [label_cut_8_5,            n2s(GGMH_HM_BDT_500, 0),       n2s(100*GGMH_HM_BDT_500/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_500, 3),        n2s(100*weighted_GGMH_HM_BDT_500/weighted_events_before_presel, 3)       ],
					 [label_cut_8_6,            n2s(GGMH_HM_BDT_600, 0),       n2s(100*GGMH_HM_BDT_600/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_600, 3),        n2s(100*weighted_GGMH_HM_BDT_600/weighted_events_before_presel, 3)       ],
					 [label_cut_8_7,            n2s(GGMH_HM_BDT_700, 0),       n2s(100*GGMH_HM_BDT_700/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_700, 3),        n2s(100*weighted_GGMH_HM_BDT_700/weighted_events_before_presel, 3)       ],
					 [label_cut_8_8,            n2s(GGMH_HM_BDT_800, 0),       n2s(100*GGMH_HM_BDT_800/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_800, 3),        n2s(100*weighted_GGMH_HM_BDT_800/weighted_events_before_presel, 3)       ],
					 [label_cut_8_9,            n2s(GGMH_HM_BDT_900, 0),       n2s(100*GGMH_HM_BDT_900/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_900, 3),        n2s(100*weighted_GGMH_HM_BDT_900/weighted_events_before_presel, 3)       ],
					 [label_cut_8_10,           n2s(GGMH_HM_BDT_1000,0),       n2s(100*GGMH_HM_BDT_1000/events_before_presel,3),        n2s(weighted_GGMH_HM_BDT_1000, 3),       n2s(100*weighted_GGMH_HM_BDT_1000/weighted_events_before_presel,3)       ]]
if dataType == "b":
	headers = [" ",                     "Unweighted",                  "Unweighted \%",                                        "Weighted",                              "Weighted \%"                                                              ]
	table = [[label_0,                  n2s(events_before_presel, 0),  n2s((100*events_before_presel/events_before_presel), 3), n2s(weighted_events_before_presel, 3), n2s(100*weighted_events_before_presel/weighted_events_before_presel, 3)    ],
					 [label_00,                 n2s(events_total, 0),          n2s((100*events_total/events_before_presel), 3),         n2s(weighted_events_total, 3),         n2s(100*weighted_events_total/weighted_events_before_presel, 3)            ],
					 #[label_cut_1,             n2s(events_cut_hh, 0),         n2s(100*events_cut_hh/events_before_presel, 3),          n2s(weighted_events_cut_hh, 3),        n2s(100*weighted_events_cut_hh/weighted_events_before_presel, 3)           ],
					 [label_cut_2,              n2s(events_pass_met, 0),       n2s(100*events_pass_met/events_before_presel, 3),        n2s(weighted_events_pass_met, 3),      n2s(100*weighted_events_pass_met/weighted_events_before_presel, 3)         ],
					 [label_cut_3,              n2s(events_cut_lep, 0),        n2s(100*events_cut_lep/events_before_presel, 3),         n2s(weighted_events_cut_lep, 3),       n2s(100*weighted_events_cut_lep/weighted_events_before_presel, 3)          ],
					 [label_cut_4,              n2s(events_cut_pflowJets, 0),  n2s(100*events_cut_pflowJets/events_before_presel, 3),   n2s(weighted_events_cut_pflowJets, 3), n2s(100*weighted_events_cut_pflowJets/weighted_events_before_presel, 3)    ],
					 [label_cut_5,              n2s(events_cut_3bJets, 0),     n2s(100*events_cut_3bJets/events_before_presel, 3),      n2s(weighted_events_cut_3bJets, 3),    n2s(100*weighted_events_cut_3bJets/weighted_events_before_presel, 3)       ],
					 [label_cut_6,              n2s(events_cut_EtMiss, 0),     n2s(100*events_cut_EtMiss/events_before_presel, 3),      n2s(weighted_events_cut_EtMiss, 3),    n2s(100*weighted_events_cut_EtMiss/weighted_events_before_presel, 3)       ],
					 [label_cut_7,              n2s(events_cut_dphimin, 0),    n2s(100*events_cut_dphimin/events_before_presel, 3),     n2s(weighted_events_cut_dphimin, 3),   n2s(100*weighted_events_cut_dphimin/weighted_events_before_presel, 3)      ],
					 [label_cut_8_1,            n2s(GGMH_HM_BDT_200, 0),       n2s(100*GGMH_HM_BDT_200/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_200, 3),      n2s(100*weighted_GGMH_HM_BDT_200/weighted_events_before_presel, 3)         ],
					 [label_cut_8_2,            n2s(GGMH_HM_BDT_250, 0),       n2s(100*GGMH_HM_BDT_250/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_250, 3),      n2s(100*weighted_GGMH_HM_BDT_250/weighted_events_before_presel, 3)         ],
					 [label_cut_8_3,            n2s(GGMH_HM_BDT_300, 0),       n2s(100*GGMH_HM_BDT_300/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_300, 3),      n2s(100*weighted_GGMH_HM_BDT_300/weighted_events_before_presel, 3)         ],
					 [label_cut_8_4,            n2s(GGMH_HM_BDT_400, 0),       n2s(100*GGMH_HM_BDT_400/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_400, 3),      n2s(100*weighted_GGMH_HM_BDT_400/weighted_events_before_presel, 3)         ],
					 [label_cut_8_5,            n2s(GGMH_HM_BDT_500, 0),       n2s(100*GGMH_HM_BDT_500/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_500, 3),      n2s(100*weighted_GGMH_HM_BDT_500/weighted_events_before_presel, 3)         ],
					 [label_cut_8_6,            n2s(GGMH_HM_BDT_600, 0),       n2s(100*GGMH_HM_BDT_600/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_600, 3),      n2s(100*weighted_GGMH_HM_BDT_600/weighted_events_before_presel, 3)         ],
					 [label_cut_8_7,            n2s(GGMH_HM_BDT_700, 0),       n2s(100*GGMH_HM_BDT_700/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_700, 3),      n2s(100*weighted_GGMH_HM_BDT_700/weighted_events_before_presel, 3)         ],
					 [label_cut_8_8,            n2s(GGMH_HM_BDT_800, 0),       n2s(100*GGMH_HM_BDT_800/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_800, 3),      n2s(100*weighted_GGMH_HM_BDT_800/weighted_events_before_presel, 3)         ],
					 [label_cut_8_9,            n2s(GGMH_HM_BDT_900, 0),       n2s(100*GGMH_HM_BDT_900/events_before_presel, 3),        n2s(weighted_GGMH_HM_BDT_900, 3),      n2s(100*weighted_GGMH_HM_BDT_900/weighted_events_before_presel, 3)         ],
					 [label_cut_8_10,           n2s(GGMH_HM_BDT_1000,0),       n2s(100*GGMH_HM_BDT_1000/events_before_presel,3),        n2s(weighted_GGMH_HM_BDT_1000, 3),     n2s(100*weighted_GGMH_HM_BDT_1000/weighted_events_before_presel,3)         ]]
if dataType == "d": 
	headers = [" ",                    "Unweighted",                 "Unweighted \%",                              ]
	table = [[label_0,                  n2s(events_total, 0),         n2s(100*events_total/events_total, 3)        ],
					 #[label_cut_1,             n2s(events_cut_hh, 0),        n2s(100*events_cut_hh/events_total, 3)       ],
					 [label_cut_2,              n2s(events_pass_met, 0),      n2s(100*events_pass_met/events_total, 3)     ],
					 [label_cut_3,              n2s(events_cut_lep, 0),       n2s(100*events_cut_lep/events_total, 3)      ],
					 [label_cut_4,              n2s(events_cut_pflowJets, 0), n2s(100*events_cut_pflowJets/events_total, 3)],
					 [label_cut_5,              n2s(events_cut_3bJets, 0),    n2s(100*events_cut_3bJets/events_total, 3)   ],
					 [label_cut_6,              n2s(events_cut_EtMiss, 0),    n2s(100*events_cut_EtMiss/events_total, 3)   ],
					 [label_cut_7,              n2s(events_cut_dphimin, 0),   n2s(100*events_cut_dphimin/events_total, 3)  ],
					 [label_cut_8_1,            n2s(GGMH_HM_BDT_200, 0),      n2s(100*GGMH_HM_BDT_200/events_total, 3)     ],
					 [label_cut_8_2,            n2s(GGMH_HM_BDT_250, 0),      n2s(100*GGMH_HM_BDT_250/events_total, 3)     ],
					 [label_cut_8_3,            n2s(GGMH_HM_BDT_300, 0),      n2s(100*GGMH_HM_BDT_300/events_total, 3)     ],
					 [label_cut_8_4,            n2s(GGMH_HM_BDT_400, 0),      n2s(100*GGMH_HM_BDT_400/events_total, 3)     ],
					 [label_cut_8_5,            n2s(GGMH_HM_BDT_500, 0),      n2s(100*GGMH_HM_BDT_500/events_total, 3)     ],
					 [label_cut_8_6,            n2s(GGMH_HM_BDT_600, 0),      n2s(100*GGMH_HM_BDT_600/events_total, 3)     ],
					 [label_cut_8_7,            n2s(GGMH_HM_BDT_700, 0),      n2s(100*GGMH_HM_BDT_700/events_total, 3)     ],
					 [label_cut_8_8,            n2s(GGMH_HM_BDT_800, 0),      n2s(100*GGMH_HM_BDT_800/events_total, 3)     ],
					 [label_cut_8_9,            n2s(GGMH_HM_BDT_900, 0),      n2s(100*GGMH_HM_BDT_900/events_total, 3)     ],
					 [label_cut_8_10,           n2s(GGMH_HM_BDT_1000,0),      n2s(100*GGMH_HM_BDT_1000/events_total,3)     ]]

from tabulate import tabulate
print()
print(weighted_GGMH_HM_BDT_250)
print(tabulate(table, headers, tablefmt="latex_raw", floatfmt=("0.f",".0f", ".3e", ".3f", ".3e")))
#print(tabulate(table, headers, tablefmt="latex_raw", floatfmt=".3e")

output = open("./tex/table.tex", "w")
output.writelines(tabulate(table, headers, tablefmt="latex_raw"))
output.writelines("\\vspace{15pt}")
output.writelines("\n\n {\\tiny Using files: \n\n")
for f in f_in:
  output.writelines(f.replace("_"," \\textunderscore ") + '\n\n')
output.writelines("\n\n Using TTree: " + tree_name.replace("_"," \\textunderscore "))
output.writelines("}")
output.close()

os.chdir("./tex/")
os.system("pdflatex ./main.tex > /dev/null 2>&1")
command = "mv main.pdf " + f_out + ".pdf"
os.system(command)
command = "cp table.tex " + f_out + ".tex"
os.system(command)

print("counter: ", counter)
print("events: ", events_total)
